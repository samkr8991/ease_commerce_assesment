const { publishToMessageQueue } = require("./utils/rabbitMQService");
const { ArgumentParser } = require('argparse');
const fs = require('fs');


const publishSensorData = async (
  towerId,
  location,
  temperature,
  powerSource,
  fuelStatus
) => {
  try {
    await publishToMessageQueue({
      towerId,
      location,
      temperature,
      powerSource,
      fuelStatus,
    });

    console.log("Sensor data published to messaging queue");
  } catch (error) {
    console.error(error.message);
  }
};

const jsonData = fs.readFileSync("./utils/sampleData.json");
const sensorDataArray = JSON.parse(jsonData);

(async () => {
  for (const sensorData of sensorDataArray) {
    const { towerId, location, temperature, powerSource, fuelStatus } = sensorData;
    await publishSensorData(towerId, location, temperature, powerSource, fuelStatus);
  }
})();

// (async () => {
//     const parser = new ArgumentParser({
//         description: 'Argparse example'
//       });
//   parser.add_argument('--towerId', { help: 'Tower ID', required: true });
//   parser.add_argument('--location', { help: 'Location', required: true });
//   parser.add_argument('--temperature', { help: 'Temperature', required: true });
//   parser.add_argument('--powerSource', { help: 'Power Source', required: true });
//   parser.add_argument('--fuelStatus', { help: 'Fuel Status', required: true });

//   const args = parser.parse_args();
 

//   const { towerId, location, temperature, powerSource, fuelStatus } = args;

//   // Call the publishSensorData function with arguments from the command line
//   await publishSensorData(towerId, location, temperature, powerSource, fuelStatus);
// })();

