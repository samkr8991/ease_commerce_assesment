const amqp = require("amqplib");

const RABBITMQ_URL = "amqp://localhost";

exports.publishToMessageQueue = async (data) => {
  try {
    const connection = await amqp.connect(RABBITMQ_URL);
    const channel = await connection.createChannel();
    const queue = "sensor_data_queue";

    await channel.assertQueue(queue, { durable: false });
    await channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));

    console.log("Message sent to RabbitMQ:", data);
  } catch (error) {
    console.error("Error publishing to RabbitMQ:", error);
  }
};

exports.readFromMessageQueue = async () => {
  try {
    const connection = await amqp.connect(RABBITMQ_URL);
    const channel = await connection.createChannel();
    const queue = "sensor_data_queue";

    const message = await new Promise((resolve, reject) => {
      channel.consume(queue, (msg) => {
        resolve(JSON.parse(msg.content.toString()));
        channel.ack(msg);
      });
    });

    console.log("Message received from RabbitMQ:", message);
    return message;
  } catch (error) {
    console.error("Error reading from RabbitMQ:", error);
  }
};