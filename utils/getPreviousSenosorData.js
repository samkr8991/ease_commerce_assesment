exports.getPreviousSensorData = async (towerId, timestamp) => {
  const twoHoursAgo = new Date(timestamp.getTime() - 2 * 60 * 60 * 1000);
  return await SensorData.find({
    towerId,
    timestamp: { $gte: twoHoursAgo, $lt: timestamp },
  });
};
