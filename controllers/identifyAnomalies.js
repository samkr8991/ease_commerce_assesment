const Anomaly = require("../models/anomaliesSchema");
const SensorData = require("../models/sensorDataSchema");

exports.identifyAnomalies = async (req, res) => {
  try {
    // Fetch the latest sensor data
    const latestSensorData = await SensorData.find()
      .sort({ timestamp: -1 })
      .limit(1);

    // Check for anomalies
    const { towerId, temperature, powerSource, fuelStatus } =
      latestSensorData[0];

    if (temperature > 45) {
      const anomaly = new Anomaly({
        towerId,
        anomalyType: "HighTemperature",
        timestamp: new Date(),
      });
      await anomaly.save();
    }

    if (fuelStatus < 20) {
      const anomaly = new Anomaly({
        towerId,
        anomalyType: "LowFuel",
        timestamp: new Date(),
      });
      await anomaly.save();
    }

    // Check for prolonged generator usage
    const prevSensorData = await SensorData.find({ towerId })
      .sort({ timestamp: -1 })
      .limit(24);
    let generatorUsageTime = 0;
    prevSensorData.forEach((data, index) => {
      if (data.powerSource === "DG") {
        generatorUsageTime += 5; // Assuming 5-second intervals
      } else {
        generatorUsageTime = 0;
      }
      if (generatorUsageTime >= 120) {
        const anomaly = new Anomaly({
          towerId,
          anomalyType: "ProlongedGeneratorUsage",
          timestamp: new Date(),
        });
        anomaly.save();
        return;
      }
    });

    res.status(200).json({ message: "Anomalies identified" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

exports.getAnomalies = async (req, res) => {
  try {
    const anomalies = await Anomaly.find();
    res.status(200).json(anomalies);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


// exports.deleteAnomalies = async (req, res) => {
//   try {
//     await Anomaly.deleteMany({});
//     res.status(200).json({ message: "Anomalies reset" });
//   } catch (error) {
//     res.status(400).json({ message: error.message });
//   }
// };
