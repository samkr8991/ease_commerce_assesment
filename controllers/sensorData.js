const Anomaly = require("../models/anomaliesSchema");
const SensorData = require("../models/sensorDataSchema");
const {
  publishToMessageQueue,
  readFromMessageQueue,
} = require("../utils/rabbitMQService");


exports.getSensorData = async (req, res) => {
  try {
    const sensor_data = await SensorData.find();
    res.status(200).json(sensor_data);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// exports.sensorData = async (req, res) => {
//   try {
//     const {
//       towerId,
//       location: { latitude, longitude },
//       temperature,
//       powerSource,
//       fuelStatus,
//     } = req.body;

//     const sensorData = new SensorData({
//       towerId,
//       temperature,
//       powerSource,
//       fuelStatus,
//     });

//     // Check for anomalies
//     if (temperature > 45) {
//       const anomaly = new Anomaly({
//         towerId,
//         anomalyType: "HighTemperature",
//         timestamp: new Date(),
//       });
//       await anomaly.save();
//     }

//     if (fuelStatus < 20) {
//       const anomaly = new Anomaly({
//         towerId,
//         anomalyType: "LowFuel",
//         timestamp: new Date(),
//       });
//       await anomaly.save();
//     }

//     // Check for prolonged generator usage
//     const prevSensorData = await SensorData.find({ towerId })
//       .sort({ timestamp: -1 })
//       .limit(24);
//     let generatorUsageTime = 0;
//     prevSensorData.forEach((data, index) => {
//       if (data.powerSource === "DG") {
//         generatorUsageTime += 5; // Assuming 5-second intervals
//       } else {
//         generatorUsageTime = 0;
//       }
//       if (generatorUsageTime >= 120) {
//         const anomaly = new Anomaly({
//           towerId,
//           anomalyType: "ProlongedGeneratorUsage",
//           timestamp: new Date(),
//         });
//         anomaly.save();
//         return;
//       }
//     });

//     await sensorData.save();
//     res.status(201).json(sensorData);
//   } catch (error) {
//     res.status(400).json({ message: error.message });
//   }
// };

// exports.readSensorData = async () => {
//   try {
//     const sensorData = await readFromMessageQueue();

//     const {
//       towerId,
//       location: { latitude, longitude },
//       temperature,
//       powerSource,
//       fuelStatus,
//     } = sensorData;

//     const newSensorData = new SensorData({
//       towerId,
//       temperature,
//       powerSource,
//       fuelStatus,
//     });
//     await newSensorData.save();

//     res.status(200).json(newSensorData);
//   } catch (error) {
//     res.status(400).json({ message: error.message });
//   }
// };
