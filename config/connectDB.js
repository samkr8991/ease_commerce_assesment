const mongoose = require("mongoose");

const connectDB = async () => {
  const db_url =
    process.env.MONGO_URI || "mongodb://localhost:27017/ease_commerce";

  let mongooseConnection = await mongoose.connect(db_url);

  console.info(`MongoDB Connected: ${mongooseConnection.connection.host}`);
};

module.exports = connectDB;
