const express = require("express");
const router = express.Router();

const {
  publishSensorData,
  readSensorData,
  sensorData,
  getSensorData
} = require("../controllers/sensorData");

const {
  identifyAnomalies,
  getAnomalies,
  deleteAnomalies,
} = require("../controllers/identifyAnomalies");

// --> Sensor Data
router.get("/sensor-data", getSensorData);
// router.post("/sensor-data", sensorData);
// router.post("/publish-sensor-data", publishSensorData);
// router.post("/read-sensor-data", readSensorData);

// --> Anomalies
router.post("/identify-anomalies", identifyAnomalies);
router.get("/anomalies", getAnomalies);
// router.delete("/anomalies", deleteAnomalies);

module.exports = router;
