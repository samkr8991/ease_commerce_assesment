const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const anomalySchema = new Schema({
  towerID: {
    type: String,
    required: true,
  },
  anomalyTpe: {
    type: String,
    required: true,
    enum: ["HighTemperature", "LowFuel", "ProlongedGeneratorUsage"],
  },
  
  timestamp: { type: Date, required: true },
});

const Anomaly = mongoose.model("anomaly", anomalySchema);

module.exports = Anomaly;
