const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sensorDataSchema = new Schema(
  {
    towerId: {
      // type: mongoose.Schema.Types.ObjectId,
      // ref: "Tower",
      type: String,
      required: true,
    },
    temperature: {
      type: Number,
      required: true,
    },
    powerSource: {
      type: String,
      enum: ["DG", "Electric"],
      required: true,
    },
    fuelStatus: {
      type: Number,
      required: true,
    },
    location: {
      latitude: { type: Number, required: true },
      longitude: { type: Number, required: true },
    },
  },
  {
    timestamps: true,
  }
);

const SensorData = mongoose.model("sensor_data", sensorDataSchema);

module.exports = SensorData;
