const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const towerSchema = new Schema(
  {
    towerID: {
      type: String,
      required: true,
      unique: true,
    },
    location: {
      latitude: { type: Number, required: true },
      longitude: { type: Number, required: true },
    },
  },
  { timestamps: true }
);

const Tower = mongoose.model("tower", towerSchema);

module.exports = Tower;
