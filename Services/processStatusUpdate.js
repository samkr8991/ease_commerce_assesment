const SensorData = require("../models/sensorDataSchema");
const { getPreviousSensorData } = require("../utils/getPreviousSenosorData");
const amqp = require("amqplib/callback_api");

class processStatusUpdate {
  constructor() {
    console.log("process update started");
  }

  async saveSensorData(sensorData) {
    try {
      const sensorDataDoc = new SensorData(sensorData);
      await sensorDataDoc.save();
      console.log("Sensor data saved to database");
    } catch (error) {
      console.error("Error saving sensor data to database:", error);
    }
  }

  async checkAnomalies(sensorData) {
    const { temperature, fuelStatus, powerSource } = sensorData;

    if (temperature > 45) {
      return true;
    }

    if (fuelStatus < 20) {
      return true;
    }

    // Check for continuous DG power source anomaly
    const previousSensorData = await getPreviousSensorData(
      sensorData.towerId,
      sensorData.timestamp
    );
    if (previousSensorData.every((data) => data.powerSource === "DG")) {
      return true;
    }

    return false;
  }

  async triggerAlarm() {}

  async startApplication() {
    rabbitMQChannel.consume(rabbitMQQueue, (msg) => {
      if (msg) {
        const sensorData = JSON.parse(msg.content.toString());
        saveSensorData(sensorData);
      }
    });

    setInterval(checkForAnomalies, 10000); // Check every 10 seconds
  }
}

module.exports = new processStatusUpdate();
