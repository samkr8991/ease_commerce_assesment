const amqp = require("amqplib");
const queue = "sensor_data";

exports.generateSensorData = async () => {
  console.log("Inside generate sensor data");
  
  amqp
    .connect("amqp://localhost")
    .then((connection) => {
      return connection.createChannel();
    })
    .then((channel) => {
      return channel.assertQueue(queue).then(() => {
        setInterval(() => {
          const sensorData = generateSensorData();
          channel.sendToQueue(queue, Buffer.from(JSON.stringify(sensorData)));
          console.log(`Sent sensor data: ${JSON.stringify(sensorData)}`);
        }, 5000);
      });
    })
    .catch((error) => {
      console.error("Error connecting to RabbitMQ:", error);
    });

  //@Desc This will generates a sensor data, where some will be anomalies also.
  function generateSensorData() {
    const towerId = `tower-${Math.floor(Math.random() * 100)}`;
    const location = {
      lat: Math.random() * 90,
      long: Math.random() * 180,
    };
    const temperature = Math.floor(Math.random() * 60);
    const powerSource = Math.random() < 0.5 ? "DG" : "Electric";
    const fuelStatus = Math.floor(Math.random() * 100);

    let anomalies = [];

    if (temperature > 45) {
      anomalies.push("High temperature");
    }

    if (fuelStatus < 20) {
      anomalies.push("Low fuel");
    }

    return {
      towerId,
      location,
      temperature,
      powerSource,
      fuelStatus,
      anomalies,
    };
  }
};
