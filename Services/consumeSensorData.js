const amqp = require("amqplib/callback_api");
const SensorData = require("../models/sensorDataSchema");

exports.consumeMessagesFromQueue = async () => {
  console.log("inside consumer queue")
  
  amqp.connect("amqp://localhost", (error0, connection) => {
    if (error0) {
      throw error0;
    }

    connection.createChannel((error1, channel) => {
      if (error1) {
        throw error1;
      }
      const queue = "sensor_data_queue";

      channel.assertQueue(queue, {
        durable: false,
      });

      console.log(
        " [*] Waiting for messages in %s. To exit, press CTRL+C",
        queue
      );

      channel.consume(
        queue,
        async (msg) => {
          const message = JSON.parse(msg.content.toString());
          console.log(" [x] Received %s", message);

          try {
            const sensorData = new SensorData({
              towerId: message.towerId,
              location: message.location,
              temperature: message.temperature,
              powerSource: message.powerSource,
              fuelStatus: message.fuelStatus,
            });

            const savedData = await sensorData.save();
            console.log("Data saved to MongoDB:", savedData);
            //   channel.ack(msg);
          } catch (error) {
            console.error("Error saving data to MongoDB:", error);
            //   channel.nack(msg);
          }
        },
        {
          noAck: false,
        }
      );
    });
  });
};
