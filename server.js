const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const connectDB = require("./config/connectDB");
const sensorDataRoutes = require("./routes/routes");
const { consumeMessagesFromQueue } = require("./Services/consumeSensorData");
const { generateSensorData } = require("./Services/generateSesnorData");


(async () => {
  connectDB();
  // consumeMessagesFromQueue()
  // generateSensorData()
})()

const app = express();
app.use(express.json());

app.use(cors());
app.use(express.urlencoded({ extended: true }));
dotenv.config({ path: "./config/.env" });

app.use("/api/v1", sensorDataRoutes);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running in ${PORT}`);
});
